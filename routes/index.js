var express = require('express');
var ptp = require('../module/parse/patent_parser');


var router = express.Router();


/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

router.get('/parse/description', function (req, res, next) {
    console.log(ptp);
    res.send(ptp.parse_dsc('text'));
});

module.exports = router;
